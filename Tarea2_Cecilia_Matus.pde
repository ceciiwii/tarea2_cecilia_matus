//Librería para cargar audio
import processing.sound.*;
//Audio
SoundFile ALERTA;


//VARIABLES
int circle_X1, circle_Y1;  // Posición del botón de aceleración
int circle_X2, circle_Y2;  // Posición del botón de frenado
int circleSize = 120;   // Diametro del botón
boolean circleOver_1 = false;
boolean circleOver_2 = false;
int ancho = 20;
int centro = 420;
//Nubes
int leftX;
int rightX;
//Osito
int posX;
int yp=500;
//Iglú
int xm=250;
int ym=450;
//Tiempos
int t = 0;
int t_ref = 100;
int t_max = 100;
int nextTime = 0;
int previousTime = 0;
//Alarma
boolean peligro =true;
int t_i=0;



void setup(){
  size(1100,900); //Tamaño
  smooth();
  noStroke(); //Bordes
  ALERTA=new SoundFile(this, "GameOverMario.mp3");
  rectMode(CENTER);
  circle_X1 = 250;
  circle_Y1 = 700;
  circle_X2 = 650;
  circle_Y2 = 700;
}

void draw()
{
  update(mouseX, mouseY);
  background(255, 105, 180); //Hot pink
  stroke(0,0,0);
  
  float posX = map(t,0,t_max,0,680);
  float velX = map(t,0,t_max,0,150);
  
  //Velocimetro visual
    //Fondo
    fill(135, 206, 250); //Ciello (light sky blue)
    stroke(0);
    rect (550, 300, 1000, 550);
    fill(60, 179, 113); //Suelo (Green)
    rect(550, 530, 1000, 100);
    //fill(160, 82, 45);
    fill(210, 180, 140);
    ellipse(950, 520, 200, 50); //Peligro (SeaGreen)
    
    //Nube
    fill(255, 255, 255);
    noStroke();
    ellipse(270, 160, 100, 60); //derecha
    ellipse(200, 150, 150, 100); //centro
    ellipse(130, 160, 100, 60); //izquierda
        
    //Cuerpo que avanza
    fill(255, 127, 80); //Tacometro
    stroke(0);
    rect(110+posX, 125, 20, 200);
    //Oso Polar Pequeño que se mueve
    //Cuerpo
        //fill(255, 255, 169);
        fill(184, 134, 11);
        noStroke();
        ellipse(110+posX,yp,90,80); //Cuerpo color amarillito
        fill(250, 250, 225);//Cuerpo color blanco
        ellipse(110+posX,yp,45,50); 
        //Cabeza
        //fill(255, 255, 169);
        //fill(165, 42, 42);
        fill(184, 134, 11);
        ellipse(110+posX,yp-50,60,50); //Cabeza
        ellipse(110+posX-20,yp-75,20,20); //Oreja izquierda
        ellipse(110+posX+20,yp-75,20,20); //Oreja derecha
        fill(0, 0, 0);
        stroke(0,0,0);
        ellipse(110+posX-10,yp-60,6,6);//Ojo izquierdo
        ellipse(110+posX+10,yp-60,6,6);//Ojo derecho
        ellipse(110+posX,yp-50,7,7);//Nariz

    //Indicador numérico
    fill(0,0,0);
    for (int i=0; i<=150; i+=10){text(str(i), 110+i*4.65, 50);}

  //Consola
  fill(0, 0 ,0);   //Negro
  rect(450,700, 600, 200);
  noStroke();
  
  //Botón Aceleración
  fill(100,149,237);
  circle(circle_X1, circle_Y1, circleSize);
  if (circleOver_1)
    {
      fill(152,255,150);  //Color encendido
    } 
  else
    {
      fill(0,52,0);  //Color apagado 
    }
  stroke(0,0,0);
  triangle(200, 720, 250, 650, 300, 720);
  
  //Botón Frenado
  fill(100,149,237);
  circle(circle_X2, circle_Y2, circleSize);
  if (circleOver_2)
    {
      fill(220,20,60);  //Color encendido 
    }
  else
    {
      fill(97,0,0);  //Color apagado 
    }
  stroke(0,0,0);
  triangle(600, 680, 650, 750, 700, 680); 
  
  //Mouse sobre botón verde ACELERADOR
  if (circleOver_1) 
    {if (peligro==true){
      t_i=millis();
      peligro=false;}
      if (millis()-t_i>10000){
      if (!ALERTA.isPlaying()){ALERTA.play();}}

      if (millis()-previousTime>= t_ref)
        {
          previousTime = millis();
          //println(t);
          t += 1;
          if (t >= t_max)
            {
              t = t_max;
            }
        }}
  else
    { peligro=true;
      if (millis()-previousTime>= t_ref)
        {
          previousTime = millis();
          t -= 1;
          if (t <= 0)
            {
              t = 0;
            }}}
  //Mouse sobre botón rojo
  if (circleOver_2)
    {
    //Mensaje sobre el botón de frenado (PELIGRO)
    textAlign(CENTER);
    textMode(35);
    fill(255, 99, 71);
    text("RETROCEDA", 950, 525);
      previousTime = 0;
      if (millis()-previousTime>= t_ref)
        {
          previousTime = millis();
          println(t);
          t -= 1;
          if (t < 0)
            {
              t = 0; }}}
  else
    {
      if (t>0)
        {
          t -= 0;}}
    
  println(t);
    
    //Mouse fuera de los botones
    if (!circleOver_1 && !circleOver_2){ALERTA.stop();} //Para que el sonido se detenga
    
    //Señalador de velocidad
    fill(255, 250, 240); //Fondo del tablero (Floralwhite)
    rect(450,700, 220, 150); 
    textAlign(CENTER);
    textSize(24);
    fill(255, 105, 180);
    text("Velocidad:", 450,700);
    text("[Km/h]", 490,725);
    textAlign(RIGHT);
    text(velX, 430,725);    
    
}

void update(int x, int y){ 
    if ( overCircle_1(circle_X1, circle_Y1, circleSize) ) {
    circleOver_1 = true;
    circleOver_2 = false;
  } else if ( overCircle_2(circle_X2, circle_Y2, circleSize) ) {
    circleOver_2 = true;
    circleOver_1 = false;
  } else {
    circleOver_1 = circleOver_2 = false;
  }
}

boolean overCircle_1(int x, int y, int diameter) {
  float disX = x - mouseX;
  float disY = y - mouseY;
  if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
    return true;
  } else {
    return false;
  }
}

boolean overCircle_2(int x, int y, int diameter) {
  float disX = x - mouseX;
  float disY = y - mouseY;
  if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
    return true;
  } else {
    return false;
  }
}
